/* global Games:true, Players:true */
'use strict';

Games = new Mongo.Collection('games');
Players = new Mongo.Collection('players');
