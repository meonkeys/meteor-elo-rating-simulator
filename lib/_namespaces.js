/* global Games:true, Players:true */

// This file exists so that we can 'use strict'; in all other files and
// still have some global namespace variables.

// It's called what it's called and placed where it's placed so that it loads
// as early as possible.

// NOTE: we cannot 'use strict'; in this file because this is where we define
// globals.

Games = {};
Players = {};
