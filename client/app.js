'use strict';

Template.registerHelper('roundToHundredth', function(val) {
  return Math.round(val * 100) / 100;
});

Template.games.helpers({
  'databaseIsEmpty': function() {
    return Games.find().count() === 0;
  },
  'games': function() {
    return Games.find({}, {sort: {number: 1}});
  }
});

Template.players.helpers({
  'players': function() {
    return Players.find({}, {sort: {rating: -1}});
  }
});

function getPlayer(name) {
  var newPlayer = {};
  var player = Players.findOne({name: name});
  if (!player) {
    newPlayer.name = name;
    newPlayer.rating = 1400;
    newPlayer.e = 0.5;
    newPlayer.gamesPlayed = 0;
    newPlayer.wins = 0;
    newPlayer.losses = 0;
    newPlayer._id = Players.insert(newPlayer);
    return newPlayer;
  }
  return player;
}

function getMatchups() {
  var rawtext = $('textarea').val();
  var matchups = [];
  var lines = rawtext.split('\n');
  _.each(lines, function(line) {
    if (!line.trim()) {
      return;
    }
    var players = line.split(',');
    matchups.push([players[0].trim(), players[1].trim()]);
  });
  return matchups;
}

function getRecord(player) {
  return player.wins + '-' + player.losses;
}

function calculateRatings(winner, loser) {
  var winnerRating = winner.rating + 32 * (1 - winner.e);
  var loserRating = loser.rating + 32 * (0 - loser.e);
  return [winnerRating, loserRating];
}

function calculateEValues(winner, loser) {
  var winnerE = 1 / (1 + Math.pow(10, (loser.rating - winner.rating) / 400));
  var loserE = 1 / (1 + Math.pow(10, (winner.rating - loser.rating) / 400));
  return [winnerE, loserE];
}

Template.games.events({
  'click #clearSimulation': function(event, template) {
    Meteor.call('clearSimulation', function(error, result) {
      if (error) {
        console.error(error);
      }
    });
  },
  'click #runSimulation': function(event, template) {
    var gameNumber = 1;
    _.each(getMatchups(), function(matchup) {
      var playerOne = getPlayer(matchup[0]);
      var playerTwo = getPlayer(matchup[1]);
      var game = {number: gameNumber, winner: {}, loser: {}};
      var ratings = calculateRatings(playerOne, playerTwo);
      playerOne.rating = ratings[0];
      playerTwo.rating = ratings[1];
      var eValues = calculateEValues(playerOne, playerTwo);

      playerOne.gamesPlayed++;
      playerOne.wins++;
      playerOne.e = eValues[0];
      game.winner = playerOne;
      game.winner.record = getRecord(playerOne);
      Players.update(playerOne._id, playerOne);

      playerTwo.gamesPlayed++;
      playerTwo.losses++;
      playerTwo.e = eValues[1];
      game.loser = playerTwo;
      game.loser.record = getRecord(playerTwo);
      Players.update(playerTwo._id, playerTwo);

      Games.insert(game);

      gameNumber++;
    });
  }
});
