'use strict';

Meteor.methods({
  'clearSimulation': function() {
    Players.remove({});
    Games.remove({});
  }
});
